# REPTE: Treball amb VirtualBox i les seves xarxes

> ATENCIÓ: Els noms dels equips hosts al VirtualBox, el nom del sistema quan instal·leu, els noms de l'usuari, equip, etc. han de ser el vostre per a que a les captures es pugui veure.

> Convertiu el fitxer Markdown en pdf abans de lliurar la pràctica, i afegiu també la carpeta d'imatges amb el document .md en un sol .zip

> Teniu l'exercici a fer al final d'aquest enunciat. Llegiu primer tot el document abans de començar la pràctica.

---

## Referències i ajuda

- Xarxes VirtualBox - https://www.pinguytaz.net/index.php/2016/11/20/virtualbox-configurando-la-red/

## Objectius

- L'objectiu d'aquesta pràctica és repassar i provar algunes característiques de VirtualBox i configurar les màquines per les següents pràctiques. Els exercicis a fer estan al final del document.

---

## Conceptes previs

- **Equip amfitrió**: La màquina física que fa còrrer el virtualBox.
- **Equip convidat**: La màquina virtual que corre sobre l'amfitrió.

---

A continuació teniu les passes que seguim normalment per a crear màquines virtuals amb VirtualBox.

0. Instal·lar Extension pack a VirtualBox
1. Baixar imatges ISO i comprovar la integritat
2. Crear màquines virtuals base o plantilla
3. Clonar màquines
4. Configurar xarxa de la màquina virtual
---

### 0. Instal·lar Extension pack a VirtualBox

Cal instal·lar **Extension Pack** al programa VirtualBox per tenir algunes funcionalitats que no venen per defecte a VirtualBox (suport de dispositius USB 2.0 i USB 3.0, Webcam, VirtualBox RDP, encriptació de disc, NVMe i PXE ROM per a targetes de xarxa Intel).
  - El podem descarregar a la web de VirtualBox https://www.virtualbox.org/wiki/Downloads i fer doble click a sobre per a que s'isntal·li o bé anar a Prferències->Extensions i buscar el fitxer.

**NOTA:** Cal instal·lar la mateixa versió de Extension Pack que tenim instal·lada de VirtualBox (veure amb l'opció "Ajuda->Quant a VirtualBox").

![](imgs/virtualbox-058f0b83.png)

### 1. Baixar imatges ISO i comprovar la integritat

Recorda **comprovar abans de fer servir les imatges ISO** que s'han baixat correctament.

- Baixa el fitxer que contenen els hash sha256. Els trobaràs a la pàgina d'on descarregues la imatge ISO (per exmple d'Ubuntu).
- Pots fer servir la comanda sha256sum nom-fitxer.
~~~
  $ sha256sum ubuntu-20.04.1-desktop-amd64.iso
 b45165ed3cd437b9ffad02a2aad22a4ddc69162470e2622982889ce5826f6e3d  ubuntu-20.04.1-desktop-amd64.iso
~~~

- També pots fer servir el programa gtkhash.

    ![](imgs/virtualbox-05bbc88c.png)

- El hash SHA256 ha de coincidir amb el que has recuperat de la web on vas baixar la imatge ISO.

---

### 2. Crear màquines virtuals base o plantilla

- Necessitem crear màquines virtuals "plantilla". Primer de tot, crearem un grup al VirtualBox (anomenat "Plantilles" o "Base") amb les màquines plantilla:
  - Ubuntu Server 20.04.
  - Ubuntu Desktop 20.04 (**Instal·lat amb les opcions mínimes**).
  - Windows 10

- Més endavant
  - Kali Linux (Vigileu, hi ha dues versions. Una live i una altra Install).
  - Windows Server
  - PfSense

---

### 2.2. Instal·lar Guest Additions a les màquines Base (si escau)

Per a que l'equip convidat pugui interaccionar amb VirtualBox, cal afegir els **Guest Additions** (depenent del sistema, cal instal·lar un paquet o un altre a la màquina convidada). Un cop arrencada la màquina, al seu menú buscar l'opció per inserir el "cd" amb les Guest Additions.

---

### 3. Clonar màquines

- Cada cop que necessitem una nova màquina virtual del mateix tipus, en lloc de tornar a instal·lar el sistema, farem un clon ENLLAÇAT.

- Picarem opcions avançades i escollirem l'opció "Clon enllaçat".
- Per a tenir clar quines màquines virtuals fem servir a cada mòdul, crearem **grups** per separar-les.
  - **Creació d'un grup**: Seleccionar màquines que volem agrupar (Mantenint tecla CTRL podem seleccionar vàries). Botó dret per a crear grup.

  ![](imgs/virtualbox-a3c4a416.png)

  - Resultat: Un grup per a M6 i un per a plantilles.

  ![](imgs/virtualbox-2b5ce73c.png)

  - Físicament, al disc dur tindrem les màquines virtuals en carpetes diferents.

  ![](imgs/virtualbox-0bf71a9d.png)

---

### 4. Configurar xarxa de la màquina virtual

- Aneu a la màquina virtual que voleu editar, piqueu botó dret, Paràmetres i aneu a Xarxa.

  ![](imgs/virtualbox-38873dcd.png)

#### NAT (diferent a "NAT Network")

- Permet que un equip virtual tingui accés a internet de forma fàcil. Es crea un router virtual que implementa NAT i tradueix les adreces dels equips convidats.
- Els equips convidats no són accessibles directament des de l'amfitrió, però es pot configurar el reenviament de ports cap a la màquina convidada i així exposar serveis específics de la màquina convidada.

  ![](imgs/virtualbox-924cbcf4.png)

- Si volem reenviar el port 8080 de la màquina amfitriona cap el 80 de la convidada, afegim una regla de reenviament com aquesta (podem afegir regles per a cada servei que volem exposar):

  ![](imgs/virtualbox-d3bed598.png)

- D'aquesta manera, quan obrim el navegador web a la **màquina amfitriona** i posem http://localhost:8080, apareixerà la pàgina web del servidor web que ha d'estar funcionant a la màquina convidada.

- Això funciona per a tots els ports que necessitem i amb els clients que necessitem, per exemple ssh. Reenviar el port 2222 del host amfitrió cap el 22 de la màquina convidada permetrà accedir via ssh a la convidada (sempre que el servei ssh estigui instal·lat) escrivint :
~~~
$ ssh -p 2222 usuari@localhost
~~~

#### Adaptador pont

Permet que l'equip convidat tingui accés a la xarxa física directament.
- Es comportarà com si fos una màquina física més i tindrà una IP de la xarxa física (si existeix un servei DHCP).
- Es pot accedir des de la màquina amfitriona i qualsevol de la xarxa física.

#### Xarxa interna

L'equip convidat NO tindrà accés a la xarxa física ni tampoc internet.
- Caldrà configurar un altre convidat per a fer de router, si calgués.
- Tampoc es pot accedir des de l'amfitrió.

#### Només Amfitrió

La xarxa permet que l'equip amfitrió pugui accedir als equips convidats.
- Es crea una nova interfície en l'equip físic que es connecta a la xarxa "només amfitrió". Així podem interactuar amb els convidats des de l'equip físic, però sense donar accés des de la xarxa física.

- Cal anar a les opcions generals de VirtualBox per a crear una nova xarxa Només Amfitrió. Aneu al menú fitxer, gestor de xarxes amfitrió.
  ![](imgs/virtualbox-9fd66e4d.png)

- Cal picar el botó "crea". Ja tindrem la xarxa només amfitrió.
  ![](imgs/virtualbox-4b6d69cd.png)

- A l'equip amfitrió es crea una nova interfície anomenada vboxnet0, que podeu comprovar que de moment no te IP assignada. VirtualBox li assignarà una adreça normalment dintre de la xarxa 192.168.56.0/24 quan el host convidat es posi en marxa:
~~~
$ ip a
...
4: vboxnet0: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether 0a:00:27:00:00:00 brd ff:ff:ff:ff:ff:ff
~~~

---

#### Opcions de xarxa avançades

En desplegar el botó "Avançades" podrem accedir a les opcions Extra:

![](imgs/virtualbox-8f78a3dc.png)

- **Tipus d'adaptador**: Per defecte Intel PRO 1000. Depenent del sistema operatiu convidat, cal escollir una altra si no la suporta.

- **Mode promiscu**: Permet que des de la màquina virtual es pugui veure el tràfic de xarxa que viatja entre terceres màquines. És a dir, és com si es connectés la interfície a un hub en lloc d'un switch. A part, caldrà posar la targeta en mode promiscu al sistema operatiu convidat si volem veure aquest tràfic (per exemple ho fa Wireshark).
  - Hi ha **tres opcions**: Denega, Permet màquines virtuals o Permet-ho tot.

- **Adreça MAC**: Permet veure quina MAC te la targeta i generar una de nova si ho necessitem.

---

## Exercicis

1. Configura el VirtualBox amb Extension Pack.
2. Crea les màquines virtuals plantilla de Ubuntu (Desktop, Server). Recorda crear un grup i comprovar els hash de les imatges ISO abans de començar.
3. Configura Guest Additions en la plantilla Ubuntu Desktop.
2. Crea un clon enllaçat de l'Ubuntu Desktop i activa la xarxa "només Amfitrió". Posa'l en marxa.
3. Mira quina adreça té l'equip convidat i quina adreça s'ha assignat a l'equip amfitrió a la interfície vobxnet0.
4. Fes ping des de la màquina amfitriona a la màquina convidada i des de la convidada a la amfitriona.
5. Connecta't via ssh a la màquina convidada des de l'amfitriona.
6. Clona un Ubuntu Server i activa la xarxa NAT. Posa'l en marxa. Comprova que tens accés a Internet.
7. Canvia l'adreça MAC del clon.
8. Instal·la el servei ssh al server.
9. Configura el reenviament de ports de la màquina virtual creada i posa 2222 al port amfitrió i 22 al port convidat. No cal posar IP ni de la convidada ni de l'amfitriona.
10. Connecta via ssh al servidor, obrint una consola a la màquina amfitriona.
